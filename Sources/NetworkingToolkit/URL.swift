import Foundation

extension URL: ExpressibleByStringLiteral {
    /// Initializes a URL from a static string.
    /// If the URL cannot be initialized it will throw a **runtime error**.
    /// - Parameter value: URL in string format
    public init(stringLiteral value: StaticString) {
        guard let url = URL(string: "\(value)") else {
            preconditionFailure("Invalid URL string literal: \(value)")
        }
        self = url
    }
}
