import Foundation

// MARK: - URLRequest.Method

public extension URLRequest {
    /// HTTP request method.
    enum Method: String {
        case get, post, put, patch, delete

        public var value: String {
            rawValue.uppercased()
        }
    }
}

// MARK: - URLRequest.Header

public extension URLRequest {
    /// HTTP request header.
    struct Header: Hashable {
        let key: String
        let value: String

        /// Initializes an HTTP header with the given `key`-`value`.
        /// - Parameter key: key
        /// - Parameter value: value
        public init(key: String, value: String) {
            self.key = key
            self.value = value
        }
    }
}

extension URLRequest.Header: Equatable {
    public static func == (lhs: URLRequest.Header, rhs: URLRequest.Header) -> Bool {
        lhs.key == rhs.key
    }
}

// MARK: - URLRequest.Builder

public extension URLRequest {
    final class Builder {
        private(set) var urlRequest: URLRequest

        /// Initializes the builder with the given `URL`.
        /// - Parameter url: url
        public init(url: URL) {
            self.urlRequest = .init(url: url)
        }
    }
}

public extension URLRequest.Builder {
    /// Sets the `cachePolicy` to use for the request.
    /// - Parameter cachePolicy: cachePolicy
    func setCachePolicy(_ cachePolicy: URLRequest.CachePolicy) -> URLRequest.Builder {
        urlRequest.cachePolicy = cachePolicy
        return self
    }

    /// Sets the `timeout` for the request.
    /// - Parameter timeout: timeout
    func setTimeout(_ timeout: TimeInterval) -> URLRequest.Builder {
        urlRequest.timeoutInterval = timeout
        return self
    }

    /// Sets the HTTP method to use.
    /// - Parameter method: HTTP method
    func setMethod(_ method: URLRequest.Method) -> URLRequest.Builder {
        urlRequest.httpMethod = method.value
        return self
    }

    /// Sets the request's headers.
    /// - Parameter headers: headers
    func setHeaders(_ headers: Set<URLRequest.Header>?) -> URLRequest.Builder {
        urlRequest.allHTTPHeaderFields = headers
            .flatMap({ headers -> [String: String] in
                Dictionary(uniqueKeysWithValues: headers.lazy.map({ ($0.key, $0.value) }))
            })
        return self
    }

    /// Sets the HTTP body.
    /// - Parameter httpBody: HTTP body
    func setBody(_ httpBody: Data?) -> URLRequest.Builder {
        urlRequest.httpBody = httpBody
        return self
    }

    /// Returns the built `URLRequest`.
    func build() -> URLRequest {
        urlRequest
    }
}
