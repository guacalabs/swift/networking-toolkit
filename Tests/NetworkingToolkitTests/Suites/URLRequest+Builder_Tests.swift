import XCTest
import NetworkingToolkit

final class URLRequest_Builder_Tests: XCTestCase {
    func test_builder() throws {
        // given
        let url: URL = "https://api.test.com/test"
        let requestMethod: URLRequest.Method = .get
        let cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy
        let timeout: TimeInterval = 60.0
        let headers: Set<URLRequest.Header> = [
            .init(key: "test-header-key", value: "test-header-value"),
            .init(key: "other-test-header-key", value: "other-test-header-value")
        ]
        let requestBody = TestData(key: "key")

        // when
        let urlRequest = URLRequest
            .Builder(url: url)
            .setMethod(requestMethod)
            .setCachePolicy(cachePolicy)
            .setTimeout(timeout)
            .setHeaders(headers)
            .setBody(try JSONEncoder().encode(requestBody))
            .build()

        // then
        XCTAssertEqual(requestMethod.value, urlRequest.httpMethod)
        XCTAssertEqual(cachePolicy, urlRequest.cachePolicy)
        XCTAssertEqual(timeout, urlRequest.timeoutInterval)
        XCTAssertEqual(urlRequest.allHTTPHeaderFields?["test-header-key"], "test-header-value")
        XCTAssertEqual(urlRequest.allHTTPHeaderFields?["other-test-header-key"], "other-test-header-value")
        XCTAssertEqual(requestBody, try urlRequest.httpBody.map({ try JSONDecoder().decode(TestData.self, from: $0) }))
    }

    func test_headers_equality() {
        // given
        let uniqueKey: String = "key"
        // when
        let headerOne = URLRequest.Header(key: uniqueKey, value: "header-one")
        let headerTwo = URLRequest.Header(key: uniqueKey, value: "header-two")
        // then
        XCTAssertEqual(headerOne, headerTwo)
    }
}

// MARK: - Test Data

private extension URLRequest_Builder_Tests {
    struct TestData: Codable, Equatable {
        let key: String
    }
}
