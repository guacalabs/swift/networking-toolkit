// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "NetworkingToolkit",
    platforms: [.iOS(.v11), .tvOS(.v11), .macOS(.v10_14)],
    products: [
        .library(
            name: "NetworkingToolkit",
            targets: ["NetworkingToolkit"]
        )
    ],
    dependencies: [
        .package(url: "https://gitlab.com/guacalabs/swift/unit-test-toolkit.git", .upToNextMajor(from: "1.0.0-beta"))
    ],
    targets: [
        .target(
            name: "NetworkingToolkit",
            dependencies: []
        ),
        .testTarget(
            name: "NetworkingToolkitTests",
            dependencies: ["NetworkingToolkit", "UnitTestToolkit"]
        )
    ],
    swiftLanguageVersions: [.v5]
)
